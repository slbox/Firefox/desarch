# DesArch+


## TODO
- [ ] Modify layout with CSS + JS
	- [ ] thumbnail of thread should be centered
	- [ ] center post
	- [ ] new themes
- [ ] Add JS features
	- [ ] image expansion
		- [x] click/touch to expanse the image
		- [ ] display loading while loading an expansion image
	- [ ] post expansion
		- [ ] auto reducing anonymous' post
		- [ ] manually expansing and reducing posts
	- [ ] compare translation with proofreading
		- [ ] pc | hover to see
		- [ ] mobile | touch to see 
	- [ ] user setting right on the desuarchive.org
		- [ ] choose themes
		- [ ] customize themes
		- [ ] default settings, such as:
			- [ ] image expansion
			- [ ] post expansion

## Troubles
- PostCSS/Surgarss got problems with some plugins
- Gulp `error: write after end at writeAfterEnd` prevents using `series`, `parallel` and `watch`