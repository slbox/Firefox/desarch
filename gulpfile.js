const isDev = false
const { src, dest, watch, series, parallel } = require('gulp')
const debug = require('gulp-debug')()


const map = {
	src: { sourcemaps: isDev },
	dest: isDev ? { sourcemaps: '.' } : null
}

const raw = {
	meta: [
		'meta/manifest.json'
	],
	style: [
		'style/layout.sss',
		'style/theme.sss'
	],
	script: 'script/*.js'
}

const output = '_pkg'

const end = dest(
	output,
	map.dest
)

const clean = () => {
	return require('del')(output)
}

const toCSS = done => {
	const postcss = require('gulp-postcss')() //github.com/postcss/postcss#gulp
	const rename = require('gulp-rename')({
		extname: '.css'
	})

	// Don't know why this fix the problem
	// but it works anyway so...
	done()
	return src(raw.style, map.src)
		.pipe(debug)
		.pipe(postcss)
		.pipe(rename)
		.pipe(debug)
		.pipe(end)
}

const toJS = done => {
	const babel = require('gulp-babel')()

	done()
	return src(raw.script, map.src)
		.pipe(debug)
		.pipe(babel)
		.pipe(end)
}

const cpMeta = () => {
	const minify = require('gulp-json-minify')()

	return src(raw.meta)
		.pipe(minify)
		.pipe(end)
}

const watcher = done => {
	// Error: write after end at writeAfterEnd
	done()
	watch(raw.style, toCSS)
	watch(raw.script, toJS)
}

const publish = () => {
	const zip = require('gulp-zip')('desarch.zip')

	return src(`${output}/*`)
		.pipe(zip)
		.pipe(dest('_pub'))
}

exports.clean = clean

exports.css = toCSS
exports.js = toJS
exports.meta = cpMeta

exports.wcss = () => watch(raw.style, toCSS)
exports.wjs = () => watch(raw.script, toJS)
exports.watch = watcher

// exports.test = publish


exports.build = series(clean, cpMeta, parallel(toCSS, toJS))// Error: write after end at writeAfterEnd
exports.publish = series(clean, cpMeta, parallel(toCSS, toJS), publish)// Error: write after end at writeAfterEnd
exports.default = series(clean, cpMeta, watcher)
