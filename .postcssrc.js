const fn = require('./style/fn/index.js') // BUG glob doesn't work


module.exports = {
	parser: 'sugarss',
	plugins: {
		// 'postcss-easy-import': {
		// 	extensions: '.sss' // BUG 
		// },
		// 'postcss-nested-import': null,
		'postcss-advanced-variables': null,
		'postcss-nested': null,
		'postcss-nested-props': null, // BUG 
		'postcss-functions': {
			functions: fn
		},
		'cssnano': {
			preset: [
				'default',
				{ discardComments: { removeAll: true } }
			]
		}
	}
}