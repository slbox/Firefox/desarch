const reset = {}
reset.thumb = () => {
	const link = () => {
		const lnks = document.querySelectorAll('a.thread_image_link')
		lnks.forEach(e => {
			e.removeAttribute('target')
		})
	}

	const img = () => {
		const rmWH = (el) => {
			el.removeAttribute('width')
			el.removeAttribute('height')
		}

		const query = className => `a.thread_image_link > img.${className}`

		const thumb = document.querySelector(query('thread_image'))
		rmWH(thumb)

		const imgs = document.querySelectorAll(query('post_image'))
		imgs.forEach(e => rmWH(e))
	}

	link()
	img()
}


const evt = {}
evt.extendImg = (lnk, e) => {
	// Do more research
	// medium.com/@leonardobrunolima/javascript-tips-common-mistake-using-closures-35d7b55f5380
	e.preventDefault()

	const img = lnk.querySelector('img.post_image') ?
		lnk.querySelector('img.post_image') :
		lnk.querySelector('img.thread_image');

	if (!img.dataset.thumb) {
		img.dataset.thumb = img.getAttribute('src')
		img.setAttribute('src', lnk.href)
	} else if (typeof img.dataset.thumb == 'string') {
		img.setAttribute('src', img.dataset.thumb)
		delete img.dataset.thumb
	}
}
evt.watch = () => {
	const lnks = document.querySelectorAll('a.thread_image_link')
	lnks.forEach(lnk => {
		lnk.onclick = e => evt.extendImg(lnk, e)
	})
}


reset.thumb()
evt.watch()